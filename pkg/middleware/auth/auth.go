package auth

import (
	"github.com/sirupsen/logrus"

	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
	"gitlab.com/fighters-solution/executioner/pkg/middleware"
)

var (
	log = logrus.WithField("pkg", "auth")
)

type (
	// Authenticator defines a flexible interface for implementing/inter-changing
	// authentication mechanisms.
	Authenticator interface {
		// Auth allows to check authentication on many levels of AuthLevel.
		// The authLevels will be sorted to preserve weakest-to-strongest
		// authentication level order.
		//
		// For example:
		// - Auth(ValidateInternalPartner, AcceptAll)
		//   => Request always be authenticated since AcceptAll has lower authentication level
		//      than ValidateInternalPartner so it always be checked first.
		Auth(roles ...model.UserRole) middleware.Middleware
	}
)
