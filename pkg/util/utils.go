package util

import (
	"strings"
	"unicode"

	"github.com/google/uuid"

	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
)

func Underscore(str string) string {
	runes := []rune(str)
	var out []rune
	for i := 0; i < len(runes); i++ {
		if i > 0 &&
			(unicode.IsUpper(runes[i]) || unicode.IsNumber(runes[i])) &&
			((i+1 < len(runes) && unicode.IsLower(runes[i+1])) || unicode.IsLower(runes[i-1])) {
			out = append(out, '_')
		}
		out = append(out, unicode.ToLower(runes[i]))
	}
	return string(out)
}

// UUID generates unique ID string
func UUID() string {
	return strings.ReplaceAll(uuid.New().String(), "-", "")
}

func UppercaseFirstChar(str string) string {
	if str == string(model.LanguageCPP) {
		return "C++"
	}
	return strings.ToUpper(str[:1]) + str[1:]
}
