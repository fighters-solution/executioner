package util

import "encoding/json"

func ToJSONString(v interface{}) string {
	b, err := json.Marshal(v)
	if err != nil {
		return `{"error": "failed to marshal to JSON string"}`
	}
	return string(b)
}
