package config

import (
	"fmt"
	"log"
	"sync"
	"time"

	"github.com/fsnotify/fsnotify"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"

	"gitlab.com/fighters-solution/executioner/pkg/util"
)

const (
	StageDevelopment Stage = "dev"
	StageProduction  Stage = "prod"

	DefaultEmailHolder = "faker@codefighter.app"

	DefaultRunnerTimeout = 60000 // 60s
	DefaultPageSize      = 50
	MaxPageSize          = 100

	DefaultConfirmTokenExpiration = 15 * time.Minute

	ConfirmURLTmpl = "https://codefighter.app/confirm?token=%s"
)

type (
	Stage string

	ConfigChanged func(Config) error

	Config struct {
		mux                *sync.Mutex
		configChangedHooks []ConfigChanged

		Server   Server   `mapstructure:"server"`
		Database Database `mapstructure:"database"`
		Redis    Redis    `mapstructure:"redis"`
		Email    Email    `mapstructure:"email"`
		Runner   Runner   `mapstructure:"runner"`
		Misc     Misc     `mapstructure:"misc"`
		Log      Log      `mapstructure:"log"`
	}

	Server struct {
		HostURL         string        `mapstructure:"host_url"`
		Address         string        `mapstructure:"address"`
		Port            string        `mapstructure:"port"`
		ReadTimeout     time.Duration `mapstructure:"read_timeout"`
		WriteTimeout    time.Duration `mapstructure:"write_timeout"`
		ShutdownTimeout time.Duration `mapstructure:"shutdown_timeout"`
		TLS             TLS           `mapstructure:"tls"`
		Security        Security      `mapstructure:"security"`
	}

	TLS struct {
		Enable   bool   `mapstructure:"enable"`
		CertFile string `mapstructure:"cert_file"`
		KeyFile  string `mapstructure:"key_file"`
	}

	Security struct {
		EnableCORS         bool    `mapstructure:"enable_cors"`
		EnableEmailConfirm bool    `mapstructure:"enable_email_confirm"`
		Session            Session `mapstructure:"session"`
	}

	Session struct {
		Secret   string        `mapstructure:"secret"`
		Domain   string        `mapstructure:"domain"`
		Path     string        `mapstructure:"path"`
		MaxAge   time.Duration `mapstructure:"max_age"`
		Secure   bool          `mapstructure:"secure"`
		HttpOnly bool          `mapstructure:"http_only"`
	}

	Database struct {
		Type    string  `mapstructure:"type"`
		MongoDB MongoDB `mapstructure:"mongodb"`
	}

	MongoDB struct {
		Addresses []string      `mapstructure:"addresses"`
		Username  string        `mapstructure:"username"`
		Password  string        `mapstructure:"password"`
		Database  string        `mapstructure:"database"`
		Timeout   time.Duration `mapstructure:"timeout"`
	}

	Redis struct {
		Address  string `mapstructure:"address"`
		Password string `mapstructure:"password"`
	}

	Email struct {
		ServerHost  string `mapstructure:"server_host"`
		ServerPort  int    `mapstructure:"server_port"`
		Username    string `mapstructure:"username"`
		Password    string `mapstructure:"password"`
		SenderName  string `mapstructure:"sender_name"`
		SenderEmail string `mapstructure:"sender_email"`
	}

	Log struct {
		Level    string `mapstructure:"level"`
		Format   string `mapstructure:"format"`
		FilePath string `mapstructure:"file_path"`
	}

	Misc struct {
		DefaultAvatar string `mapstructure:"default_avatar"`
	}

	Runner struct {
		CPU          int `mapstructure:"cpu"`
		MemoryMB     int `mapstructure:"memory_mb"`
		MemorySwapMB int `mapstructure:"memory_swap_mb"`
	}
)

// Read provides configuration being read from the given path and state
func Read(stage Stage, cfgPath string) (conf *Config, err error) {
	conf = &Config{
		mux: &sync.Mutex{},
	}
	cfgName := fmt.Sprintf("config.%s", stage)

	vn := viper.New()
	vn.AddConfigPath(cfgPath)
	vn.SetConfigName(cfgName)

	if err := vn.ReadInConfig(); err != nil {
		return nil, errors.Wrapf(err, "failed to read config file")
	}
	if err := conf.binding(vn); err != nil {
		return nil, errors.Wrapf(err, "failed to bind to config file")
	}

	vn.WatchConfig()
	vn.OnConfigChange(func(e fsnotify.Event) {
		logrus.Infoln("config file changed:", e.Name)
		if err := conf.binding(vn); err != nil {
			logrus.Panicf("binding error: %v", err)
		}
	})

	return conf, nil
}

func (c *Config) OnConfigChange(e ...ConfigChanged) {
	c.mux.Lock()
	c.configChangedHooks = append(c.configChangedHooks, e...)
	c.mux.Unlock()
}

func (c *Config) binding(vn *viper.Viper) error {
	c.mux.Lock()
	defer c.mux.Unlock()

	if err := vn.Unmarshal(&c); err != nil {
		return errors.Wrapf(err, "unmarshal config error")
	}
	logrus.Infof("current config: %s", c)

	// Notify config changes to all subscribers
	for _, hook := range c.configChangedHooks {
		if err := hook(*c); err != nil {
			logrus.Errorf("failed to notify config changes to subscriber: %s", err)
		}
	}
	return nil
}

// Custom stringer to print config in JSON with obfuscated passwords/secrets.
// Value receiver so this func always process on the copy of real config object.
func (c Config) String() string {
	if c.Database.MongoDB.Password != "" {
		c.Database.MongoDB.Password = "obfuscated"
	}
	if c.Server.Security.Session.Secret != "" {
		c.Server.Security.Session.Secret = "obfuscated"
	}
	return util.ToJSONString(c)
}

func ParseAppStage(s string) Stage {
	switch s {
	case "dev", "develop", "development", "d":
		return StageDevelopment
	case "prod", "production", "p":
		return StageProduction
	default:
		log.Panicf("invalid running stage: %v", s)
	}
	return ""
}

// Should move this func to other place later
func GetPageSize(limit int) int {
	if limit <= 0 {
		return DefaultPageSize
	}
	if limit > MaxPageSize {
		return MaxPageSize
	}
	return limit
}
