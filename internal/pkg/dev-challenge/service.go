package dev_challenge

import (
	"encoding/json"
	"net/http"
	"strings"
	"time"

	"github.com/globalsign/mgo/bson"
	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"

	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
	"gitlab.com/fighters-solution/executioner/internal/session"
	"gitlab.com/fighters-solution/executioner/pkg/util"
	"gitlab.com/fighters-solution/executioner/pkg/util/request"
	"gitlab.com/fighters-solution/executioner/pkg/util/response"
)

var log = logrus.WithField("pkg", "dev-challenge")

type (
	Service struct {
		devChallengeRepo Repo
	}

	Repo interface {
		CreateChallenge(dc *model.DevChallenge) (created *model.DevChallenge, err error)
		GetChallenge(id string) (dc *model.DevChallenge, err error)
		ListChallenges(query interface{}, offset, limit int) (dcs []*model.DevChallenge, err error)
		UpdateChallenge(id string, updater interface{}) (dc *model.DevChallenge, err error)
		DeleteChallenge(id string) (err error)
		GetChallengeNames(cids []string) (cnames []challengeName, err error)
	}

	challengeName struct {
		ID    string `json:"id" bson:"id"`
		Title string `json:"title" bson:"title"`
	}
)

func NewService(devChallengeRepo Repo) *Service {
	return &Service{
		devChallengeRepo: devChallengeRepo,
	}
}

func (s *Service) CreateChallenge(w http.ResponseWriter, r *http.Request) {
	dc := model.DevChallenge{}
	if err := json.NewDecoder(r.Body).Decode(&dc); err != nil {
		log.Errorf("failed to decode challenge from create request: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	dc.ID = util.UUID()
	now := time.Now().Unix()
	dc.CreatedAt = now
	dc.UpdatedAt = now
	created, err := s.devChallengeRepo.CreateChallenge(&dc)
	if err != nil {
		log.Errorf("failed to save dev challenge: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, created)
}

func (s *Service) GetChallenge(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "cid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	dc, err := s.devChallengeRepo.GetChallenge(id)
	if err != nil {
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}

	dc.OmitSensitiveData() // Hide fixtures and test cases before returning

	if !dc.Disabled {
		response.JSON(w, http.StatusOK, dc)
		return
	}

	// If challenge is currently disabled then also hide challenge description from response,
	// except request from admin or challenge's owner
	u, err := session.GetUser(r)
	if err != nil {
		log.Errorf("getFullChallenge error: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	if model.UserRole(u.Role) == model.Admin || u.UID == dc.PostedBy {
		response.JSON(w, http.StatusOK, dc)
		return
	}
	dc.Description = ""
	response.JSON(w, http.StatusOK, dc)
}

func (s *Service) GetFullChallenge(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "cid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	dc, err := s.devChallengeRepo.GetChallenge(id)
	if err != nil {
		log.Errorf("failed to get challenge: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}

	// Prevent the case when user fake the URL's userID param to match their token's userID
	// => Pass auth owner.
	// Double check to verify that user is the owner of challenge.
	u, err := session.GetUser(r)
	if err != nil {
		log.Errorf("getFullChallenge error: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	uid := chi.URLParam(r, "uid")
	if model.UserRole(u.Role) != model.Admin && (uid != "" && dc.PostedBy != uid) {
		response.ErrorCode(w, http.StatusUnauthorized)
		return
	}
	response.JSON(w, http.StatusOK, dc)
}

func (s *Service) ListChallenges(w http.ResponseWriter, r *http.Request) {
	offset, limit := request.QueryInt(r, "offset"), config.GetPageSize(request.QueryInt(r, "limit"))
	cids := strings.Split(request.Query(r, "cids"), ",")
	query := bson.M{}
	if strings.TrimSpace(request.Query(r, "cids")) != "" && len(cids) > 0 {
		query = bson.M{"id": bson.M{"$in": cids}}
	}

	dcs, err := s.devChallengeRepo.ListChallenges(query, offset, limit)
	if err != nil {
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}

	type dcWrapper struct {
		*model.DevChallenge
		Languages []string `json:"languages"`
	}
	dcws := make([]dcWrapper, len(dcs))
	for i := range dcs {
		dcws[i].Languages = make([]string, 0)
		for _, code := range dcs[i].Codes {
			dcws[i].Languages = append(dcws[i].Languages, code.Language)
		}
		dcs[i].Description = ""
		dcs[i].OmitSensitiveData()
		dcws[i].DevChallenge = dcs[i]
	}
	response.JSON(w, http.StatusOK, dcws)
}

// Note: This API accepts any input from user's request body then update directly to the matched dev-challenge document which is quite bad and insecure.
// Consider to build up update query or allow to update on specific fields later.
func (s *Service) UpdateChallenge(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "cid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	dc, err := s.devChallengeRepo.GetChallenge(id)
	if err != nil {
		log.Errorf("failed to get challenge: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}

	// Prevent the case when user fake the URL's userID param to match their token's userID
	// => Pass auth owner.
	// Double check to verify that user is the owner of challenge.
	u, err := session.GetUser(r)
	if err != nil {
		log.Errorf("updateChallenge error: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	uid := chi.URLParam(r, "uid")
	if model.UserRole(u.Role) != model.Admin && (uid != "" && dc.PostedBy != uid) {
		response.ErrorCode(w, http.StatusUnauthorized)
		return
	}

	updater := make(map[string]interface{})
	if err := json.NewDecoder(r.Body).Decode(&updater); err != nil {
		log.Errorf("failed to decode body from update API: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	updater["updatedAt"] = time.Now().Unix()
	updated, err := s.devChallengeRepo.UpdateChallenge(id, updater)
	if err != nil {
		log.Errorf("failed to update dev challenge: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, updated)
}

func (s *Service) DeleteChallenge(w http.ResponseWriter, r *http.Request) {
	id := chi.URLParam(r, "cid")
	if id == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	if err := s.devChallengeRepo.DeleteChallenge(id); err != nil {
		log.Errorf("failed to delete dev challenge: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, "")
}

func (s *Service) ResolveChallengeName(w http.ResponseWriter, r *http.Request) {
	cids := make([]string, 0)
	if err := json.NewDecoder(r.Body).Decode(&cids); err != nil {
		log.Errorf("failed to decode body to list of challenge IDs: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	if len(cids) == 0 {
		response.JSON(w, http.StatusOK, "")
		return
	}

	cnames, err := s.devChallengeRepo.GetChallengeNames(cids)
	if err != nil {
		log.Errorf("failed to resolve challenge names: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, cnames)
}
