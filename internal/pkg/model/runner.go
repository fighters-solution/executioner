package model

type RunnerOutput struct {
	Stdout     string `json:"stdout"`
	Stderr     string `json:"stderr"`
	Status     string `json:"status"`
	ExitCode   int    `json:"exitCode"`
	ExitSignal string `json:"exitSignal"` // nullable
	WallTime   int    `json:"wallTime"`
	OutputType string `json:"outputType"`
}
