package model

const (
	Owner UserRole = iota // 0 - Special role for auth
	Admin
	_ // Reserved for future uses
	_
	_
	Moderator
	_
	_
	_
	NormalUser
	_
	_
	_
	Anonymous
)

type (
	UserRole int

	User struct {
		ID        string   `json:"id" bson:"id"`
		UID       string   `json:"uid" bson:"uid"`
		Password  string   `json:"-" bson:"password"`
		Avatar    string   `json:"avatar" bson:"avatar"`
		Email     string   `json:"email" bson:"email" validate:"required,email"` // Current display email
		Name      string   `json:"name" bson:"name" validate:"required,max=254"`
		Role      UserRole `json:"role" bson:"role" validate:"required"`
		Team      string   `json:"team" bson:"team" validate:"max=254"`
		Location  string   `json:"location" bson:"location" validate:"max=254"`
		Company   string   `json:"company" bson:"company" validate:"max=254"`
		About     string   `json:"about" bson:"about" validate:"max=1000"`
		Cf        Cf       `json:"cf" bson:"cf"`
		Settings  Settings `json:"settings" bson:"settings"`
		Confirm   Confirm  `json:"-" bson:"confirm"`
		CreatedAt int64    `json:"createdAt" bson:"createdAt"`
		UpdatedAt int64    `json:"updatedAt" bson:"updatedAt"`
	}

	Cf struct {
		Solved    int   `json:"solved" bson:"solved"`
		Xp        int   `json:"xp" bson:"xp"`
		UpdatedAt int64 `json:"updatedAt" bson:"updatedAt"`
	}

	Settings struct {
		Language   string `json:"language" bson:"language"`
		HideSolved bool   `json:"hideSolved" bson:"hideSolved"`
		HideEmail  bool   `json:"hideEmail" bson:"hideEmail"`
		Editor     Editor `json:"editor" bson:"editor"`
	}

	Confirm struct {
		Email            string `json:"email" bson:"email"` // Email that need to be confirmed
		IsEmailConfirmed bool   `json:"isEmailConfirmed" bson:"isEmailConfirmed"`
	}

	Editor struct {
		Theme    string `json:"theme" bson:"theme"`
		FontSize string `json:"fontSize" bson:"fontSize"`
	}

	UserSession struct {
		UID  string `json:"uid"`
		Role int    `json:"role"`
		Name string `json:"name"`
	}
)

func (u *User) OmitPassword() {
	u.Password = ""
}

func (u *User) OmitEmail() {
	if u.Settings.HideEmail {
		u.Email = ""
	}
}
