package model

const (
	CookieSession = "cf_be_session"
	CookieConfirm = "cf_confirm"

	LanguageC          Language = "c"
	LanguageCPP        Language = "cpp"
	LanguageGo         Language = "go"
	LanguageJava       Language = "java"
	LanguageJavascript Language = "javascript"
	LanguageKotlin     Language = "kotlin"
	LanguagePython3    Language = "python3"
)

type Language string
