package model

type (
	League struct {
		ID          string       `json:"id" bson:"id"`
		Title       string       `json:"title" bson:"title"`
		Start       int64        `json:"start" bson:"start"`
		End         int64        `json:"end" bson:"end"`
		IsActive    bool         `json:"isActive" bson:"isActive"`
		Description string       `json:"description" bson:"description"`
		Banner      string       `json:"banner" bson:"banner"`
		Challenges  []string     `json:"challenges" bson:"challenges"`
		Competitors []Competitor `json:"competitors" bson:"competitors"`
		CreatedAt   int64        `json:"createdAt" bson:"createdAt"`
		UpdatedAt   int64        `json:"updatedAt" bson:"updatedAt"`
	}

	Competitor struct {
		UID           string  `json:"uid" bson:"uid"`
		Point         int     `json:"point" bson:"point"`
		Solved        int     `json:"solved" bson:"solved"`
		LastSolved    int64   `json:"lastSolved" bson:"lastSolved"`
		TotalExecTime float64 `json:"totalExecTime" bson:"totalExecTime"`
	}
)
