package user_solved_challenge

import (
	"encoding/json"
	"net/http"
	"time"

	"github.com/globalsign/mgo"
	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"

	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
	"gitlab.com/fighters-solution/executioner/pkg/util"
	"gitlab.com/fighters-solution/executioner/pkg/util/request"
	"gitlab.com/fighters-solution/executioner/pkg/util/response"
)

var (
	log = logrus.WithField("pkg", "user-solved-challenge")
)

type (
	Service struct {
		uscRepo Repo
	}

	Repo interface {
		CreateUserSolvedChallenge(usc *model.UserSolvedChallenge) (created *model.UserSolvedChallenge, err error)
		GetUserSolvedChallenge(uid, cid string) (usc *model.UserSolvedChallenge, err error)
		GetSolution(uid, cid, sid string) (solution *model.Version, err error)
		ListSolvedChallengesByUserID(uid string, offset, limit int) (uscs []*model.UserSolvedChallenge, err error)
		UpdateUserSolvedChallenge(id string, updater interface{}) (usc *model.UserSolvedChallenge, err error)
		DeleteUserSolvedChallenge(uid, cid string) (err error)
	}
)

func NewService(uscRepo Repo) *Service {
	return &Service{
		uscRepo: uscRepo,
	}
}

func (s *Service) CreateUserSolvedChallenge(w http.ResponseWriter, r *http.Request) {
	usc := model.UserSolvedChallenge{}
	if err := json.NewDecoder(r.Body).Decode(&usc); err != nil {
		log.Errorf("failed to decode create API body to user: %v", err)
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	// TODO: Validate

	usc.ID = util.UUID()
	now := time.Now().Unix()
	usc.CreatedAt = now
	usc.UpdatedAt = now
	created, err := s.uscRepo.CreateUserSolvedChallenge(&usc)
	if err != nil {
		log.Errorf("failed to save user solved challenge: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, created)
}

func (s *Service) GetUserSolvedChallenge(w http.ResponseWriter, r *http.Request) {
	uid := chi.URLParam(r, "uid")
	cid := chi.URLParam(r, "cid")
	if uid == "" || cid == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	usc, err := s.uscRepo.GetUserSolvedChallenge(uid, cid)
	if err == mgo.ErrNotFound {
		response.JSON(w, http.StatusOK, nil)
		return
	}
	if err != nil {
		log.Errorf("failed to get user solved challenge: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, usc)
}

func (s *Service) GetSolution(w http.ResponseWriter, r *http.Request) {
	uid := chi.URLParam(r, "uid")
	cid := chi.URLParam(r, "cid")
	sid := chi.URLParam(r, "sid")
	log.Infoln(uid, cid, sid)
	if uid == "" || cid == "" || sid == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	solution, err := s.uscRepo.GetSolution(uid, cid, sid)
	if err != nil {
		log.Errorf("failed to get user solution: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, solution)
}

func (s *Service) ListSolvedChallengesByUserID(w http.ResponseWriter, r *http.Request) {
	s.listUSCByUserID(w, r, true)
}

func (s *Service) ListFullSolvedChallengesByUserID(w http.ResponseWriter, r *http.Request) {
	s.listUSCByUserID(w, r, false)
}

func (s *Service) listUSCByUserID(w http.ResponseWriter, r *http.Request, isOmitSolutions bool) {
	uid := chi.URLParam(r, "uid")
	if uid == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	offset, limit := request.QueryInt(r, "offset"), config.GetPageSize(request.QueryInt(r, "limit"))
	uscs, err := s.uscRepo.ListSolvedChallengesByUserID(uid, offset, limit)
	if err != nil {
		log.Errorf("failed to list user solved challenges: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}

	if isOmitSolutions {
		for i := range uscs {
			uscs[i].Versions = nil
		}
	}
	response.JSON(w, http.StatusOK, uscs)
}

func (s *Service) DeleteUserSolvedChallenge(w http.ResponseWriter, r *http.Request) {
	uid := chi.URLParam(r, "uid")
	cid := chi.URLParam(r, "cid")
	if uid == "" || cid == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	if err := s.uscRepo.DeleteUserSolvedChallenge(uid, cid); err != nil {
		log.Errorf("failed to delete user solved challenge: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, "")
}
