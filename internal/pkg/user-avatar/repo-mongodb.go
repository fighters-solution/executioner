package user_avatar

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
)

const (
	userAvatars = "user_avatars" // Collection name
)

type MongoDBRepo struct {
	session *mgo.Session
}

func NewMongoDBRepo(session *mgo.Session) *MongoDBRepo {
	return &MongoDBRepo{
		session: session,
	}
}

func (r *MongoDBRepo) GetUserAvatar(id string) (avatar *model.UserAvatar, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	avatar = &model.UserAvatar{}
	err = sess.DB("").C(userAvatars).Find(bson.M{"id": id}).One(avatar)
	if err != nil {
		return nil, err
	}
	return avatar, nil
}

func (r *MongoDBRepo) UpdateUserAvatar(id string, updater interface{}) (avatar *model.UserAvatar, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	_, err = sess.DB("").C(userAvatars).Upsert(bson.M{"id": id}, updater)
	if err != nil {
		return nil, err
	}
	return r.GetUserAvatar(id)
}

func (r *MongoDBRepo) DeleteUserAvatar(id string) (err error) {
	sess := r.session.Clone()
	defer sess.Close()

	return sess.DB("").C(userAvatars).Remove(bson.M{"id": id})
}
