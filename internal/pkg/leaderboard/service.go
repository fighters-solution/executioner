package leaderboard

import (
	"net/http"
	"sort"

	"github.com/go-chi/chi"
	"github.com/sirupsen/logrus"

	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
	"gitlab.com/fighters-solution/executioner/pkg/util/response"
)

var (
	log = logrus.WithField("pkg", "leaderboard")
)

type (
	Service struct {
		leaderboardRepo Repo
		leagueRepo      LeagueRepo
	}

	Repo interface {
		GetGlobalLeaderboard() (lb []*model.User, err error)
		GetChallengeLeaderboard(cid string) (lb []*model.UserSolvedChallenge, err error)
		GetChallengeLeaderboardByTimeRange(cid string, start, end int64) (lb []*model.UserSolvedChallenge, err error)
	}

	LeagueRepo interface {
		GetLeague(lid string) (league *model.League, err error)
	}
)

func NewService(leaderboardRepo Repo, leagueRepo LeagueRepo) *Service {
	return &Service{
		leaderboardRepo: leaderboardRepo,
		leagueRepo:      leagueRepo,
	}
}

// TODO: Paging, need to check frontend first
func (s *Service) GetGlobalLeaderboard(w http.ResponseWriter, r *http.Request) {
	lb, err := s.leaderboardRepo.GetGlobalLeaderboard()
	if err != nil {
		log.Errorf("failed to get global leaderboard: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, lb)
}

func (s *Service) GetChallengeLeaderboard(w http.ResponseWriter, r *http.Request) {
	cid := chi.URLParam(r, "cid")
	if cid == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	lb, err := s.leaderboardRepo.GetChallengeLeaderboard(cid)
	if err != nil {
		log.Errorf("failed to get challenge leaderboard: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}
	response.JSON(w, http.StatusOK, lb)
}

func (s *Service) GetLeagueLeaderboard(w http.ResponseWriter, r *http.Request) {
	lid := chi.URLParam(r, "lid")
	if lid == "" {
		response.ErrorCode(w, http.StatusBadRequest)
		return
	}

	league, err := s.leagueRepo.GetLeague(lid)
	if err != nil {
		log.Errorf("failed to get league: %v", err)
		response.ErrorCode(w, http.StatusInternalServerError)
		return
	}

	ldbs := make([][]*model.UserSolvedChallenge, 0, len(league.Challenges))
	for _, challengeID := range league.Challenges {
		ldb, err := s.leaderboardRepo.GetChallengeLeaderboardByTimeRange(challengeID, league.Start, league.End)
		if err != nil {
			log.Errorf("failed to get challenge leaderboard by time range: %v", err)
			response.ErrorCode(w, http.StatusInternalServerError)
			return
		}
		ldbs = append(ldbs, ldb)
	}

	ldbByUID := groupLeaderboardsByUserId(ldbs)
	response.JSON(w, http.StatusOK, ldbByUID)
}

type UserLDB struct {
	UID        string `json:"uid"`
	Solved     int    `json:"solved"`
	TotalPoint int    `json:"total_point"`
}

func groupLeaderboardsByUserId(ldbs [][]*model.UserSolvedChallenge) []UserLDB {
	ldbByUID := make(map[string]UserLDB)

	for _, ldb := range ldbs {
		for _, solvedChallenge := range ldb {
			uldb, ok := ldbByUID[solvedChallenge.UID]
			if !ok {
				uldb = UserLDB{
					UID:        solvedChallenge.UID,
					Solved:     1,
					TotalPoint: solvedChallenge.Point,
				}
			} else {
				uldb.Solved++
				uldb.TotalPoint += solvedChallenge.Point
			}
			ldbByUID[solvedChallenge.UID] = uldb
		}
	}

	ldbArr := make([]UserLDB, 0, len(ldbByUID))
	for _, v := range ldbByUID {
		ldbArr = append(ldbArr, v)
	}
	sort.Slice(ldbArr, func(i, j int) bool {
		if ldbArr[i].TotalPoint == ldbArr[j].TotalPoint {
			return ldbArr[i].Solved < ldbArr[j].Solved
		}
		return ldbArr[i].TotalPoint < ldbArr[j].TotalPoint
	})
	return ldbArr
}
