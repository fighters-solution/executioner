package user

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
)

const (
	users = "users" // Collection name
)

type MongoDBRepo struct {
	session *mgo.Session
}

func NewMongoDBRepo(session *mgo.Session) *MongoDBRepo {
	return &MongoDBRepo{
		session: session,
	}
}

func (r *MongoDBRepo) CreateUser(u *model.User) (created *model.User, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	if err := sess.DB("").C(users).Insert(u); err != nil {
		return nil, err
	}
	created, err = r.GetUser(u.UID)
	if err != nil {
		return nil, err
	}
	created.OmitPassword()
	return created, nil
}

func (r *MongoDBRepo) GetUser(id string) (u *model.User, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	u = &model.User{}
	err = sess.DB("").C(users).Find(bson.M{"uid": id}).One(u)
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (r *MongoDBRepo) GetUserByQuery(query interface{}) (u *model.User, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	u = &model.User{}
	err = sess.DB("").C(users).Find(query).One(u)
	if err != nil {
		return nil, err
	}
	return u, nil
}

func (r *MongoDBRepo) UpdateUser(id string, updater interface{}) (u *model.User, err error) {
	sess := r.session.Clone()
	defer sess.Close()

	err = sess.DB("").C(users).Update(bson.M{"uid": id}, bson.M{"$set": updater})
	if err != nil {
		return nil, err
	}
	return r.GetUser(id)
}
