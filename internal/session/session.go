package session

import (
	"encoding/json"
	"log"
	"net/http"
	"sync"

	"github.com/gorilla/sessions"
	"github.com/pkg/errors"
	"gopkg.in/boj/redistore.v1"

	"gitlab.com/fighters-solution/executioner/internal/config"
	"gitlab.com/fighters-solution/executioner/internal/pkg/model"
)

var (
	store    sessions.Store
	once     sync.Once
	sessConf config.Session
)

func InitStore(conf config.Session, redisConf config.Redis) sessions.Store {
	once.Do(func() {
		redisStore, err := redistore.NewRediStore(100, "tcp", redisConf.Address, redisConf.Password, []byte(conf.Secret))
		if err != nil {
			log.Panicf("failed to init redis session store: %v", err)
		}
		redisStore.Options = &sessions.Options{
			Domain:   conf.Domain,
			Path:     conf.Path,
			MaxAge:   int(conf.MaxAge.Seconds()),
			Secure:   conf.Secure,
			HttpOnly: conf.HttpOnly,
		}
		sessConf = conf
		store = redisStore
	})
	return store
}

func Get(r *http.Request, key string) (*sessions.Session, error) {
	return store.Get(r, key)
}

func Set(w http.ResponseWriter, r *http.Request, sess *sessions.Session) error {
	return store.Save(r, w, sess)
}

func SetCookieOnly(w http.ResponseWriter, name, value string) {
	http.SetCookie(w, &http.Cookie{
		Name:     name,
		Value:    value,
		Domain:   sessConf.Domain,
		Path:     sessConf.Path,
		MaxAge:   int(sessConf.MaxAge.Seconds()),
		HttpOnly: false,
		Secure:   false,
	})
}

func New(r *http.Request, name string) (*sessions.Session, error) {
	return store.New(r, name)
}

func GetUser(r *http.Request) (*model.UserSession, error) {
	sess, err := Get(r, model.CookieSession)
	if err != nil {
		return nil, errors.Wrapf(err, "failed to get session")
	}
	b, ok := sess.Values["user"]
	if !ok {
		return nil, errors.Errorf("user info not found in session")
	}
	us := model.UserSession{}
	if err := json.Unmarshal([]byte(b.(string)), &us); err != nil {
		return nil, errors.Wrapf(err, "failed to decode user info from session")
	}
	return &us, nil
}
